import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from copy import deepcopy
import time


class MobileRobotModel(object):
    def __init__(self, initial_position, initial_orientation):
        self.position = initial_position
        self.orientation = initial_orientation
        self.lin_vel_magnitude = 0  # the magnitude of linear velocity
        self.rot_vel = 0  # the rotational velocity with sign

    def predict_and_update_motion(self, lin_vel_magnitude, rot_vel, time_step, init_pos=None, init_orientation=None):
        self.update_lin_rot_velocities(lin_vel_magnitude, rot_vel)
        position, orientation = self.predict_motion(time_step, init_pos=init_pos, init_orientation=init_orientation)
        self.update_position_and_orientation(new_position=position, new_orientation=orientation)
        return

    def get_states(self):
        state_dict = {"position": self.position,
                      "orientation": self.orientation,
                      "lin_vel_magnitude": self.lin_vel_magnitude,
                      "rot_vel": self.rot_vel}
        return state_dict

    def set_states(self, state_dict: dict):
        self.position = state_dict["position"]
        self.orientation = state_dict["orientation"]
        self.lin_vel_magnitude = state_dict["lin_vel_magnitude"]
        self.rot_vel = state_dict["rot_vel"]
        return

    def predict_motion(self, time_step, init_pos=None, init_orientation=None):
        """
        compute the next position and orientatoin of the robot using a fixed time step
        :param init_orientation:
        :param init_pos: use this argument in case you wanna indicated a manual initial pose
        :param time_step:
        :return:
        """
        if init_pos is not None:
            self.position = init_pos
            self.orientation = init_orientation

        projection_vector = np.array([np.cos(self.orientation), np.sin(self.orientation)]).reshape((-1, 1))
        position = self.position + projection_vector * self.lin_vel_magnitude * time_step
        orientation = self.orientation + self.rot_vel * time_step
        return position, orientation

    def update_position_and_orientation(self, new_position, new_orientation):
        self.orientation = new_orientation
        self.position = new_position

    def update_lin_rot_velocities(self, new_lin_vel, new_rot_vel):
        self.lin_vel_magnitude = new_lin_vel
        self.rot_vel = new_rot_vel


class ReferencePath(object):
    def __init__(self, spacing, acceleration, max_velocity):
        self.acceleration = acceleration
        self.max_velocity = max_velocity
        self.spacing = spacing
        self.max_vel_point_index = self._find_max_vel_point_index()
        self.points = None
        self.vel_profile = None
        self.vectors = None
        self.lengths_squared = None

    def _construct_linear_waypoints(self, start_point, end_point):
        """

        :param start_point:
        :param end_point:
        :return:
        """

        start_point_ndarray = np.array(start_point).reshape((-1, 1))
        end_point_ndarray = np.array(end_point).reshape((-1, 1))
        distance = np.linalg.norm(end_point_ndarray - start_point_ndarray)
        normal_vector = (end_point_ndarray - start_point_ndarray) / distance
        total_mid_points = int(distance / self.spacing)
        # finding the points that lay in between of the starting point and the ending point
        points_in_between = start_point_ndarray + np.arange(self.spacing, distance, self.spacing) * normal_vector
        constructed_points = np.hstack((start_point_ndarray, points_in_between, end_point_ndarray))
        return constructed_points

    def _modify_starting_velocity(self, vel_profile):
        vel_profile[0, :int(self.max_vel_point_index / 2)] = self.max_velocity / 2
        return vel_profile

    def _construct_velocity_profile(self, constructed_points_shape):
        vel_profile = np.ones((1, constructed_points_shape[1] - 1)) * self.max_velocity
        accel_length = self.max_velocity ** 2 / (2 * self.acceleration)
        accel_vel_profile = np.sqrt((np.arange(0, accel_length, self.spacing) * (2 * self.acceleration))).reshape(
            (1, -1))
        deccel_vel_profile = np.flip(accel_vel_profile)
        vel_profile[0, :self.max_vel_point_index + 1] = accel_vel_profile
        vel_profile[0, -(self.max_vel_point_index + 1):] = deccel_vel_profile
        vel_profile = self._modify_starting_velocity(vel_profile)
        return vel_profile

    def _find_max_vel_point_index(self):
        index = int(self.max_velocity ** 2 / (2 * self.acceleration * self.spacing))
        return index

    def _calculate_vectors_and_length_squared(self):
        self.vectors = np.diff(self.points)

        # calculate the squared magnitude of each vector
        self.lengths_squared = np.sum(self.vectors ** 2, axis=0)
        return

    def construct_reference_path(self, path_type="linear", **kwargs):
        """

        :param path_type: linear is implemented
        :param kwargs: if linear is used, make sure to pass start point end point and spacing as the keyword arguments
        :return: constructed points as an array of 2 by n.
        """
        constructed_points = None
        if path_type == "linear":
            constructed_points = \
                self._construct_linear_waypoints(kwargs["start_point"], kwargs["end_point"])

        self._add_points(constructed_points)
        self._calculate_vectors_and_length_squared()
        velocity_profile = self._construct_velocity_profile(constructed_points.shape)
        self._add_velocity_profile(velocity_profile)

    def _add_points(self, new_points):
        if self.points is None:
            self.points = new_points

        else:
            self.points = np.hstack((self.points, new_points))

    def _add_velocity_profile(self, new_vel_prof):
        if self.vel_profile is None:
            self.vel_profile = new_vel_prof

        else:
            self.points = np.hstack((self.vel_profile, new_vel_prof))

    def get_reference_orientation(self, closest_point_index):
        """
        having received the index of the closest point on the reference path, we can find the reference angle of
        robot.
        :param closest_point_index:
        :return: reference angle gamma
        """
        if closest_point_index == (self.points.shape[1] - 1):
            p0 = self.points[:, -2]
            p1 = self.points[:, -1]
        else:

            p0 = self.points[:, closest_point_index]
            p1 = self.points[:, closest_point_index + 1]

        delta_x = p1[0] - p0[0]
        delta_y = p1[1] - p0[1]
        reference_angle = np.arctan2(delta_y, delta_x)
        return reference_angle

    def get_reference_velocity(self, closest_point_index):
        """
        having received the index of the closest point on the reference path, we can find the reference angle of
        robot.
        :param: closest_point_index:
        :return: velocity refenrece
        """
        if closest_point_index == (self.points.shape[1] - 1):
            reference_velocity = self.vel_profile[0, -1]
        else:

            reference_velocity = self.vel_profile[0, closest_point_index]

        return reference_velocity


class Controller(object):
    def __init__(self, time_step):
        self.time_step = time_step

    def compute_vel_command(self, robot: MobileRobotModel, reference_path: ReferencePath):
        pass

    @staticmethod
    def _distance_to_polyline(waypoints_object, independent_point):
        # calculate the vector between each pair of adjacent points
        points_array = waypoints_object.points
        vectors = waypoints_object.vectors
        x = points_array[0]
        y = points_array[1]
        # calculate the squared magnitude of each vector
        lengths_squared = waypoints_object.lengths_squared

        # calculate the dot product between each vector and the independent point
        dot_products = np.sum(vectors * (independent_point - points_array[:, :-1]), axis=0)

        # calculate the parameter t for each line segment
        ts = np.maximum(0, np.minimum(1, dot_products / lengths_squared))

        # calculate the closest point on each line segment to the independent point
        closest_points = points_array[:, :-1] + vectors * ts

        # calculate the distance between each closest point and the independent point
        distances = np.sqrt(np.sum((closest_points - independent_point) ** 2, axis=0))
        min_distance_index = np.argmin(distances)
        min_dist = distances[min_distance_index]
        sign = np.sign(independent_point[1, 0] - np.interp(independent_point[0, 0], x, y))
        # return the minimum distance
        return min_distance_index, min_dist, sign

    @staticmethod
    def _compute_angle_error(robot_orientation, reference_orientation):
        angle_error = reference_orientation - robot_orientation
        angle_error = np.mod(angle_error + np.pi, 2 * np.pi) - np.pi
        return angle_error


class PID(Controller):
    def __init__(self, time_step, kp, kd, ki, look_ahead_dist, position_disturbance, orientation_disturbance):
        super().__init__(time_step=time_step)
        self.kp = kp
        self.kd = kd
        self.ki = ki
        self.accumulated_error = 0
        self.ld = look_ahead_dist
        self.position_disturbance = position_disturbance
        self.orientation_disturbance = orientation_disturbance

    def compute_vel_command(self, robot: MobileRobotModel, reference_path: ReferencePath):
        actual_position = robot.position
        actual_orientation = robot.orientation
        disturbed_position = actual_position + np.random.normal(loc=0, scale=self.position_disturbance, size=(2, 1))
        disturbed_orientation = actual_orientation + np.random.normal(loc=0, scale=self.orientation_disturbance)
        min_dist_index, min_dist, sign = super()._distance_to_polyline(reference_path, disturbed_position)
        signed_lateral_error = -sign * min_dist
        reference_orientation = reference_path.get_reference_orientation(min_dist_index)
        reference_velocity = reference_path.get_reference_velocity(min_dist_index)

        orientation_error = super()._compute_angle_error(disturbed_orientation, reference_orientation)
        rot_vel = self.kp * signed_lateral_error + self.kd * robot.lin_vel_magnitude * np.sin(orientation_error) + \
                  self.ki * self.accumulated_error * self.time_step

        self.accumulated_error += signed_lateral_error
        return reference_velocity, rot_vel


class MPC(Controller):
    def __init__(self, time_step, pred_horizon, state_weights, input_weights, goal_weights, proximity_weights,
                 input_bounds, position_disturbance, orientation_disturbance):
        super().__init__(time_step=time_step)
        self.pred_horizon = pred_horizon
        self.Q = state_weights
        self.R = input_weights
        self.Qf = goal_weights
        self.P = self._create_proximity_weight_matrix(proximity_weights)
        self.bounds = input_bounds
        self.bounds_for_optim = self._make_bounds_for_optimization()
        self.initial_guess = self._make_initial_guess()
        self.last_input_commands = np.zeros((self.R.shape[0] * self.pred_horizon, 1), dtype=float)
        self.position_disturbance = position_disturbance
        self.orientation_disturbance = orientation_disturbance

    def _make_initial_guess(self):
        init_guess = np.zeros(self.pred_horizon * 2)
        init_guess[:self.pred_horizon] = self.bounds["max_vel"]
        return init_guess

    def _create_proximity_weight_matrix(self, proximity_weight_dict: dict):
        proximity_matrix_diagonals = np.ones((self.R.shape[0] * self.pred_horizon), dtype=float)
        proximity_matrix_diagonals[:self.pred_horizon] *= proximity_weight_dict["linear_velocity"]
        proximity_matrix_diagonals[self.pred_horizon:] *= proximity_weight_dict["rot_velocity"]
        proximity_weights_matrix = np.diag(proximity_matrix_diagonals)
        return proximity_weights_matrix

    def _make_bounds_for_optimization(self):
        horizon = self.pred_horizon
        bounds = []
        lin_vel_bounds = (self.bounds["min_vel"], self.bounds["max_vel"])
        omega_bounds = (self.bounds["min_omega"], self.bounds["max_omega"])
        for i in range(horizon):
            bounds.append(lin_vel_bounds)
        for i in range(horizon):
            bounds.append(omega_bounds)
        return bounds

    def _compute_state_cost(self, position_error, orientation_error):
        """
        :param: Position_error: the minimum distance of the robot to the reference path
        Compute l_state = e' @ Q @ e
        :return:
        """
        absolute_orientation_error = np.abs(orientation_error)
        state_error_vector = np.array([position_error, absolute_orientation_error]).reshape((-1, 1))
        state_cost = state_error_vector.transpose() @ self.Q @ state_error_vector
        return np.squeeze(state_cost)

    def _compute_input_command_cost(self, input_command, reference_input):
        input_command_error = input_command - reference_input
        if type(input_command) == np.ndarray:
            input_command_error = input_command_error.reshape((-1, 1))
            input_command_cost = input_command_error.transpose() @ self.R @ input_command_error
        else:
            input_command_cost = (input_command_error ** 2) * self.R
        return input_command_cost

    def _compute_distance_to_goal_cost(self, position: np.ndarray, reference_path: ReferencePath):
        goal_position = reference_path.points[:, -1].reshape((-1, 1))
        dist_to_goal = position - goal_position
        dist_to_goal_cost = dist_to_goal.transpose() @ self.Qf @ dist_to_goal
        return np.squeeze(dist_to_goal_cost)

    def _compute_stage_cost(self, reference_path: ReferencePath, position: np.ndarray, orientation, input_command):
        """
        Compute l(x, u) = ||x-x_ref||^2_Q + ||u-u_ref||^2_R
        :return:
        """
        position = position.reshape((-1, 1))
        min_dist_index, min_dist, _ = super()._distance_to_polyline(reference_path, position)
        reference_orientation = reference_path.get_reference_orientation(min_dist_index)
        orientation_error = super()._compute_angle_error(orientation, reference_orientation)
        state_cost = self._compute_state_cost(min_dist, orientation_error)
        input_cost = self._compute_input_command_cost(input_command, reference_input=0)
        stage_cost = state_cost + input_cost
        return stage_cost

    def _compute_proximity_term_cost(self, input_commands: np.ndarray):
        input_commands_vector = input_commands.reshape((self.R.shape[0] * self.pred_horizon, 1))
        input_commands_deviation = self.last_input_commands - input_commands_vector
        proximity_cost = input_commands_deviation.transpose() @ self.P @ input_commands_deviation
        return proximity_cost

    def _compute_total_cost_set_over_horizon(self, input_commands: np.ndarray, robot: MobileRobotModel,
                                             reference_path: ReferencePath):
        original_states = robot.get_states()
        input_commands = input_commands.reshape((2, -1))
        horizon = input_commands.shape[1]
        total_cost = 0
        # getting the disturbed states in the next line
        robot.position = robot.position + np.random.normal(loc=0, scale=self.position_disturbance, size=(2, 1))
        robot.orientation = robot.orientation + np.random.normal(loc=0, scale=self.orientation_disturbance)
        to_goal_cost = self._compute_distance_to_goal_cost(robot.position, reference_path)
        for i in range(horizon):
            position = robot.position
            orientation = robot.orientation
            input_command = input_commands[:, i]
            lin_vel_command = input_command[0]
            rot_vel_command = input_command[1]
            total_cost += self._compute_stage_cost(reference_path, position, orientation, input_command)
            robot.predict_and_update_motion(lin_vel_command, rot_vel_command, time_step=self.time_step,
                                            init_pos=position, init_orientation=orientation)
        proximity_cost = self._compute_proximity_term_cost(input_commands)
        total_cost = total_cost + to_goal_cost + proximity_cost
        robot.set_states(original_states)
        return total_cost

    def compute_vel_command(self, robot: MobileRobotModel, reference_path: ReferencePath):
        optimal_inputs_results = minimize(self._compute_total_cost_set_over_horizon,
                                          self.initial_guess, (robot, reference_path),
                                          method="SLSQP",
                                          bounds=self.bounds_for_optim)
        optimal_inputs_over_horizon = optimal_inputs_results.x.reshape((2, -1))
        self.initial_guess = optimal_inputs_results.x
        self.last_input_commands = optimal_inputs_results.x.reshape((self.R.shape[0] * self.pred_horizon, 1))
        optimal_inputs = optimal_inputs_over_horizon[:, 1]
        lin_vel = optimal_inputs[0]
        rot_vel = optimal_inputs[1]
        return lin_vel, rot_vel


class Simulator(object):
    def __init__(self, time_step, maximum_time, robot: MobileRobotModel, reference_path: ReferencePath,
                 controller: Controller):
        self.time = 0
        self.time_step = time_step
        self.maximum_time = maximum_time
        self.robot = robot
        self.reference_path = reference_path
        self.controller = controller
        self.robot_position_log = []
        self.robot_orientation_log = []
        self.lin_vel_command_log = []
        self.rot_vel_command_log = []

    def simulate_control_loop(self):
        while self.time <= self.maximum_time:
            self.robot_position_log.append(deepcopy(self.robot.position))
            self.robot_orientation_log.append(deepcopy(self.robot.orientation))
            lin_vel_command, rot_vel_command = self.controller.compute_vel_command(self.robot, self.reference_path)
            self.lin_vel_command_log.append(lin_vel_command)
            self.rot_vel_command_log.append(rot_vel_command)
            self.robot.predict_and_update_motion(lin_vel_command, rot_vel_command, self.time_step)
            self.time += self.time_step
        # self.robot_position_log = np.array(self.robot_position_log).reshape((2, -1))
        self.robot_position_log = np.concatenate(self.robot_position_log, axis=1)
        self.robot_orientation_log = np.array(self.robot_orientation_log)
        self.lin_vel_command_log = np.array(self.lin_vel_command_log)
        self.rot_vel_command_log = np.array(self.rot_vel_command_log)

    def plot_position_logs(self):
        x_ref = self.reference_path.points[0, :]
        y_ref = self.reference_path.points[1, :]
        robot_x = self.robot_position_log[0, :]
        robot_y = self.robot_position_log[1, :]
        fig, axe = plt.subplots()
        fig.set_size_inches(12, 8)
        axe.plot(x_ref, y_ref, color='lime', linewidth=3.5, linestyle="-.", label="Reference Path")
        axe.plot(robot_x, robot_y, color='b', linewidth=3.5, linestyle="--", label="Robot Position")
        axe.grid()
        plt.xticks(fontsize=17, fontname="P052", fontweight='bold')
        plt.yticks(fontsize=17, fontname="P052", fontweight='bold')
        axe.set_xlabel("X [m]", fontname="P052", fontsize=20, fontweight='bold')
        axe.set_ylabel("Y [m]", fontname="P052", fontsize=20, fontweight='bold')
        # axe.xaxis.set_ticks(np.arange(x_gt.min(), x_gt.max() + 0.1, 2))
        axe.yaxis.set_ticks(np.arange(robot_y.min() - 0.1, robot_y.max() + 0.1, 0.1))

        axe.yaxis.set_ticks(np.arange(-0.1, 0.3, 0.1))

        axe.legend(loc='upper right', fontsize=15)
        axe.set_aspect('equal', adjustable='box')
        # axe.set_aspect('auto')
        plt.savefig("robot_position.svg", format="svg", dpi=900)
        plt.show()

    def plot_orientation_logs(self):
        t = np.arange(0, self.maximum_time, self.time_step)
        orientation_log = self.robot_orientation_log
        fig, axe = plt.subplots()
        fig.set_size_inches(12, 8)
        axe.plot(t, orientation_log, color='r', linewidth=3.5, linestyle="--", label="Robot Orinetation")
        axe.grid()
        plt.xticks(fontsize=17, fontname="P052", fontweight='bold')
        plt.yticks(fontsize=17, fontname="P052", fontweight='bold')
        axe.set_xlabel("Time [s]", fontname="P052", fontsize=20, fontweight='bold')
        axe.set_ylabel("Theta [rad]", fontname="P052", fontsize=20, fontweight='bold')
        axe.legend(loc='upper right', fontsize=15)
        plt.savefig("robot_orientation.svg", format="svg", dpi=900)
        plt.show()

    def plot_velocity_command_logs(self, log_to_plot="linear velocity"):
        t = np.arange(0, self.maximum_time, self.time_step)
        if log_to_plot == "linear velocity":
            log = self.lin_vel_command_log
            label = "V[m/s]"
        elif log_to_plot == "rotational velocity":
            log = self.rot_vel_command_log
            label = "Omega[rad/sec]"
        fig, axe = plt.subplots()
        fig.set_size_inches(12, 8)
        axe.plot(t, log, color='b', linewidth=3.5, linestyle="-", label=label)
        axe.grid()
        plt.xticks(fontsize=17, fontname="P052", fontweight='bold')
        plt.yticks(fontsize=17, fontname="P052", fontweight='bold')
        axe.set_xlabel("Time [s]", fontname="P052", fontsize=20, fontweight='bold')
        axe.set_ylabel(label, fontname="P052", fontsize=20, fontweight='bold')
        filename = (label + ".svg").replace("[m/s]", "").replace("[rad/sec]", "")
        plt.savefig(filename, format="svg", dpi=900)
        plt.show()


def main():
    # Simulation parameters
    initial_position = np.array([0, 0.2]).reshape((-1, 1))
    initial_orientation = 1
    spacing = 0.1
    max_velocity = 0.3  # m/s
    acceleration = 0.1  # m/s^2

    time_step = 0.1
    maximum_time = 25

    # PID controller gains
    kp = 5
    kd = 2.5
    ki = 0
    ld = 1
    position_disturbance = 0.0
    orientation_disturbance = 0.0

    # MPC controller parameters
    Q = np.array([[10, 0],
                  [0, 0.3]])

    Qf = np.array([[0.05, 0],
                   [0, 0.05]])

    R = np.array([[0, 0],
                  [0, 0.1]])

    input_bound_dict = {
        "min_omega": -0.5,
        "max_omega": +0.5,
        "min_vel": 0,
        "max_vel": 1
    }

    proximity_weights_dict = {
        "linear_velocity": 2,
        "rot_velocity": 3
    }

    robot = MobileRobotModel(initial_position, initial_orientation)
    reference_path = ReferencePath(spacing, acceleration, max_velocity)
    reference_path.construct_reference_path(path_type="linear", start_point=(0, 0), end_point=(3, 0))
    controller = MPC(time_step, pred_horizon=10, state_weights=Q, input_weights=R, goal_weights=Qf,
                     proximity_weights=proximity_weights_dict,
                     input_bounds=input_bound_dict, position_disturbance=position_disturbance,
                     orientation_disturbance=orientation_disturbance)
    # controller = PID(time_step=time_step, kp=kp, kd=kd, ki=ki, look_ahead_dist=ld,
    #                  position_disturbance=position_disturbance, orientation_disturbance=orientation_disturbance)
    simulator = Simulator(time_step, maximum_time, robot, reference_path, controller)
    simulator.simulate_control_loop()
    simulator.plot_position_logs()
    simulator.plot_orientation_logs()
    simulator.plot_velocity_command_logs(log_to_plot="linear velocity")
    simulator.plot_velocity_command_logs(log_to_plot="rotational velocity")


if __name__ == "__main__":
    main()
